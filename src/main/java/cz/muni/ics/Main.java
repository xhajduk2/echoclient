package cz.muni.ics;

public class Main {
    public static void main(String[] args) {
        String serverIP = "78.128.211.58";
        int serverPort = 4444;

        EchoClient client = new EchoClient(serverIP, serverPort);
        client.communicate();
    }
}