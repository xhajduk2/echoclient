package cz.muni.ics;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

public class EchoClient {
    private Socket socket;

    public EchoClient(String serverIP, int serverPort) {
        try {
            socket = new Socket(serverIP, serverPort);
        } catch (UnknownHostException e) {
            System.out.println("Invalid IP address or unreachable server");
            System.exit(1);
        } catch (IOException e) {
            System.out.println("An I/O error occurred while connecting to the server");
            System.exit(1);
        } catch (SecurityException e) {
            System.out.println("Security manager does not allow connection to the server");
            System.exit(1);
        } catch (IllegalArgumentException e) {
            System.out.println("Invalid server port number (between 0 and 65535)");
            System.exit(1);
        }
    }

    public void communicate() {
        try (
                BufferedReader serverOutput = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                PrintWriter serverInput = new PrintWriter(socket.getOutputStream(), true);
                BufferedReader userReader = new BufferedReader(new InputStreamReader(System.in))
        ) {
            System.out.println("Enter message to send (type exit to exit the program):");
            String userInput;
            while ((userInput = userReader.readLine()) != null) {
                serverInput.println(userInput);

                if ("exit".equalsIgnoreCase(userInput)) {
                    break;
                }

                String response = serverOutput.readLine();
                System.out.println("Server response: " + response);
            }
            socket.close();
        } catch (IOException e) {
            System.out.println("An I/O error occurred while opening input streams");
            System.exit(1);
        }
    }
}
