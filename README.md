Simple command line client for communication with echo server on 78.128.211.58:4444.

Project is written in Java 17.

Can be compiled with "maven clean install" command in the root project directory.\
To start the application execute "java -jar target/echoer-1.0-SNAPSHOT.jar" command in the root project directory.